#include "stdafx.h"
#include <iostream>

using namespace std;

int avg_int(int a, int b, int c) {
	_asm {
		movsx eax, a;
		add eax, b;
		add eax, c;
		mov ecx, 3;
		mov edx, 0;
		div ecx;
	}
}

short avg_short(short a, short b, short c) {
	_asm {
		movsx ax, a;
		add ax, b;
		add ax, c;
		mov cx, 3;
		mov dx, 0;
		div cx;
	}
}

int sgn(int i) {
	_asm {
		mov eax, 1;
		cmp eax, i;
		jle return;
		mov eax, 0;
		cmp eax, i;
		je return;
		mov eax, -1;
		return: 
	}
}

int min3(unsigned char a, short b, int c) {
	_asm {
		mov al, a;
		movsx bx, b;
		movsx ecx, c;
		cbw; // al -> ax
		cmp ax, bx;
		jle ccc;
		mov ax, bx;
	ccc:
		cwde; // ax -> eax
		cmp eax, ecx;
		jle return;
		mov eax, ecx;
	return:
	}

}

int positive(int a, int b, int c) {
	_asm {
		mov eax, 0;
		cmp eax, a;
		jg return;
		cmp eax, b;
		jg return;
		cmp eax, c;
		jg return;
		mov eax, 1;
		return:
	}
}

int power(int n, unsigned int m) {
	_asm {
		mov eax, n;
		mov ecx, m;
	for:
		cmp ecx, 1;
		jle return;
		mul n;
		dec ecx;
		jmp for;
	return:

	}
}

int main() {

	std::cout << avg_int(2, -1, 5) << std::endl; // 2
	std::cout << avg_short(-1, 2, 5) << std::endl; // 2
	std::cout << sgn(-10) << ", " << sgn(0) << ", " << sgn(10) << std::endl; // -1,0,1
	std::cout << min3(1, -4, 5) << std::endl; // -4
	std::cout << positive(-1, 2, 3) << std::endl; // 0
	std::cout << power(2, 4) << std::endl; // 16
	return 0;
}
