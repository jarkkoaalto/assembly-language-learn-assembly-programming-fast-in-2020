#include "FirstExercise.h"

#include<iostream>
using namespace std;

int power(int x) {
	_asm {
		mov eax, x
		imul eax, x
		mov x, eax
	}
	return x;
}

int main() {
 cout<<power(2)<<std::endl;
 return 0;
} 