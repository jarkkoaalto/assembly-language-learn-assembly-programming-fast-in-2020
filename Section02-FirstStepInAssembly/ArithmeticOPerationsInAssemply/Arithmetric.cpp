#include "Arithmetric.h"

#include<iostream>
using namespace std;

/**
	Write in Assembly function :  int perimeter_of_rectangle (int a, int b), which calculates perimeter of rectangle.
	Write in Assembly function :  int area_of_rectangle (int a, int b) that calculates the area of rectangle.
	Write in Assembly function :  int perimeter_of_square (int a), which calculates perimeter of square.
	Write in Assembly function :  int area_of_square (int a) that calculates the area of square.
	Write in Assembly function :  int perimeter_of_triangle (int a, int b, int c), which calculates perimeter of triangle.
	Write in Assembly function :  int perimeter_of_triangle2 (int a), which calculates perimeter of equilateral  triangle
	Write in Assembly function :  int perimeter_of_triangle3 (int a, int b), which calculates perimeter of rectangular triangle.
	Write in Assembly function :  int perimeter_of_triangle4 (int a, int h), which calculates perimeter of triangle  from side length and height.
	Write in Assembly function :  int area_of_cube (int a), which calculates area of cube.
	Write a program that calculates the content of the triangle according to the Heron formula:
	P = sqrt (s * (s-a) * (s-b) * (s-c)), s = (a + b + c) /
	2. Enter to the program a, b, c (in C language), then the calculation is done in the Assembler to the final sqrt operation,
	which you execute using the sqrt function in C (you must insert the math.h header file).Write the result (using printf in C).
	*/

int perimeter_of_rectangle(int a, int b) {
	// 2a + 2b
	_asm {
		mov eax, a;
		add eax, a;
		add eax, b;
		add eax, b;
		mov a, eax;
	}
	return a;
}
int area_of_rectangle(int a, int b) {
	// a * b
	_asm {
		mov eax, a;
		mul b;
		mov a, eax;
	}
	return a;

}
int perimeter_of_square(int a) {
	// 4
	_asm {
		mov eax, a;
		mov ecx, 4; // you must put integer in the register
		mul ecx; // and then call register
		mov a, eax;
	}
	return a;
}

int area_of_square(int a) {
	// a * a
	_asm {
		mov eax, a;
		mul a;
		mov a, eax;
	}
	return a;
}

int perimeter_of_triangle(int a, int b, int c) {
	// a+b+c
	_asm {
		mov eax, a;
		add eax, b;
		add eax, c;
		mov a, eax;
	}
	return a;
}
int perimeter_of_triangle2(int a) {
	// 3a
	_asm {
		mov eax, a;
		mov ecx, 3;
		mul ecx;
		mov a, eax;
	}
	return a;
}
int perimeter_of_triangle3(int a, int b) {
	// sqrt(a*a + b*b) + a+b
	int result = 0;
	int temp = 0;
	_asm {
		mov eax, a;
		mul a;
		mov ecx, eax;
		mov eax, b;
		mul b;
		add eax, ecx;
		mov temp, eax;
		mov eax, a;
		mov eax, b;
		mov result, eax;
	}
	result += sqrt(temp);
	return result;
}
int perimeter_of_triangle4(int a, int b, int c) {
	// P  = sqrt(s * (s-a)*(s-b)*(s-c)), s = (a+b+c) / 2
	int result = 0;
	_asm {
		mov eax, a;
		add eax, b;
		add eax, c;
		mov ecx, 2;
		mov edx, 0;
		div ecx;
		mov ecx, eax;
		sub ecx, a;
		mul ecx;
		add ecx, a;
		sub ecx, b;
		mul ecx;
		add ecx, b;
		sub ecx, c;
		mul ecx;
		mov result, eax;
	
	}
	return sqrt(result);
}
int area_of_cube(int a) {
	_asm {}
	return a;
}

int main() {
	std::cout<<perimeter_of_rectangle(2, 4)<< std::endl;
	std::cout << area_of_rectangle(2,6) << std::endl;
	std::cout << perimeter_of_square(4) << std::endl;
	std::cout << area_of_square(3) << std::endl;
	std::cout << perimeter_of_triangle(2,3,4) << std::endl;
	std::cout << perimeter_of_triangle2(5) << std::endl;
	std::cout << perimeter_of_triangle3(3, 4) << std::endl;
	std::cout << perimeter_of_triangle4(5, 6,4) << std::endl;
	/*std::cout << area_of_cube(9) << std::endl;*/
	return 0;
	
}
