#include "MemoryAccess.h"
#include <iostream>

using namespace std;

static int first[10];
static short second[10];
static short third[10];
int *fourth;
int* fifth;

void multiples(short n) {
	_asm {
		mov eax, 0;
		mov ax, n;
		mov ecx, 0;
	for:
		mov[first + 4 * ecx], eax;
		add ax, n;
		inc ecx;
		cmp ecx, 10;
		jl for;
	}
}

void coundown() {
	_asm {
		mov ax, 10;
		mov ecx, 0;
	for:
		mov[second + 2 * ecx], ax;
		dec ax;
		inc ecx;
		cmp ecx, 10;
		jl for;
	}
}

void powers() {
	_asm {
		mov ecx, 0;
		mov dx, 0;
	for:
		mov ax, cx;
		inc ax;
		mul ax;
		mov[third + 2 * ecx], ax;
		inc ecx;
		cmp ecx, 10;
		jl for;

	}
}

void powers2() {
	const int two = 2;
	_asm {
		mov ebx, fourth;
		mov ecx, 0;
		mov eax, 1;
		mov ebx, 0;
	for:
		mov[ebx + 4 * ecx], eax;
		mul two;
		inc ecx;
		cmp ecx, 10;
		jl for;
	}
}

int avg(unsigned int n) {
	_asm {
		mov ebx, fifth;
		mov ecx, 0;
		mov eax, 0;
	for:
		add eax, [ebx + 4 * ecx];
		inc ecx;
		cmp ecx, 10;
		jl for;
		mov edx, 0;
		div n;
	}
}

int minimum() {
	int local[10] = { 10,15,1,28,-18,5,20,1,3 };
	_asm {
		movsx eax, [local];
		mov ecx, 0;
	for:
		cmp ecx, 10;
		jge return;
		mov edx, [local + 4 * ecx];
		inc ecx;
		cmp eax, edx;
		jle for;
		mov eax, edx;
		jmp for;
	return:
	}
}

void print_array(int arr[], int n) {}

void print_array(short arr[], int n) {}

int main() {
	fourth = (int*)malloc(10 * sizeof(int));
	fifth = (int*)malloc(10 * sizeof(int));
	fifth[0] = 11;
	fifth[1] = 6;
	fifth[2] = 2;
	fifth[3] = 11;
	fifth[4] = 6;
	fifth[5] = 6;
	fifth[6] = 18;
	fifth[7] = 4;
	fifth[8] = -11;
	fifth[9] = -1;


	print_array(first, 10);
	multiples(4);
	coundown();
	print_array(second, 10);
	powers();
	print_array(third, 10);
	powers2();
	print_array(fourth, 10);
	std::cout << avg(10) << std::endl;
	std::cout << minimum() << std::endl;

	return 0;
}