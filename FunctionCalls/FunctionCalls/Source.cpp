#include<iostream>
#include<string.h>

using namespace std;

int factorial(int n) {
	_asm {
		mov eax, n;
		cmp eax, 1;
		je return;
		dec eax;
		push eax;
		call factorial;
		add esp, 4;
		mul n;
	return:
	}
}

char* my_strdup(char* s) {
	_asm {
		push s;
		call strlen;
		add esp, 4;
		push eax;
		call malloc;
		pop ecx;
		mov ebx, s;
	for:
		mov dl, [ebx + ecx];
		mov[eax + ecx], dl;
		dec ecx;
		cmp ecx, 0;
		jge for;
	}
	
}

unsigned int fib(unsigned short n) {
	_asm {
		movzx eax, n;
		cmp eax, 1;
		jle return;
		dec eax;
		push eax;
		call fib;
		pop ecx;
		dec ecx;
		push eax;
		push ecx;
		call fib;
		add esp, 4;
		pop edx;
		add eax, edx;
	return:
	}
}

void print_fact(unsigned char n) {
	const char* f = "fact(%d) = %d\n";
	_asm {
		movzx eax, n;
		push eax;
		call factorial;
		pop edx;
		push eax;
		push edx;
		push f;
		call printf;
		add esp, 12;
	}
}

int main() {

	char* s = (char*)malloc(5 * sizeof(char));
	s[0] = 'a';
	s[1] = 'b';
	s[2] = 'c';
	s[3] = 'd';
	s[4] = 'e';
	s[5] = 0;

	cout << factorial(4) << endl;
	cout << my_strdup(s) << endl; // abcde
	cout << fib(6)<<endl; // 8
	print_fact(4); // facr(4) = 24
	return 0;
}